# Thinking

## Analysis

#### Objects

 1. RomanNumeral
 2. Calculator
 3. InputReader
 4. OutputWriter
 5. Processor

另外对于星际单位和交易货物均以String类型替代。

#### Calculator

Calculator:

1. IntergalacticUnitCalculator(interface)

    1. add unit roman map entry
    2. compute value of unit

2. MerchandiseCalculator(interface)

    1. add merchandise exchanges notes
    2. compute how many credits do unit merchandise value

## Design and Assumptions

#### Design

此程序由主要由以下组件组成:

1. 星际单位计算器
2. 商品价格计算器
3. 输入组件
4. 输出组件
5. 核心处理组件

上述组件均包含接口类和实现类。

此程序的核心处理组件实现类MerchantGuideProcessorImpl，包含1-4四个组件的一个实例。

其它组合部分：

1. 罗马数实体
2. 程序入口
3. 计算器工厂
4. 异常

其中，计算器工厂采用工厂方法模式，提供两种计算器对象的创建方法。

#### Assumptions

程序假设如下：

1. 星际单位与罗马符号对应关系数据的数据均合法
2. 如果输入的星际单位与罗马数的数据存在相同的星际单位重复设置，则覆盖就数据
3. 不存在Credits计算结果存在小数情况
4. 假设测试数据的输入依次顺序为:
    1. 单位与罗马数对应关系
    2. 商品交易笔记记录
    3. 输入查询输入

假设不满足时的处理方式：

1. 如果假设1不满足，如果有组合形式，需要重新实现星际单位计算器
2. 如果Credits结果允许小数，需修改处理器实现，输出小数结果
3. 如果测试数据输入顺序不符合4,对于无法处理的数据将输出对应提示

