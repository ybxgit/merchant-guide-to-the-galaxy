package two8g.galaxy.calculator;

import two8g.galaxy.exception.UnknownMerchandiseException;

/**
 * Created by two8g on 17-4-13.
 */
public interface MerchandiseCalculator {
    void computeMerchandiseExchanges(String s);

    int[] getMerchandiseUnitCredit(String name) throws UnknownMerchandiseException;
}
