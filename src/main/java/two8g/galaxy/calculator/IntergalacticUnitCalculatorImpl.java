package two8g.galaxy.calculator;

import two8g.galaxy.GalaxyNoteConstants;
import two8g.galaxy.RomanNumeral;
import two8g.galaxy.exception.GalaxyException;
import two8g.galaxy.exception.UnknownUnitException;

import java.util.HashMap;

/**
 * Created by two8g on 17-4-13.
 */
public class IntergalacticUnitCalculatorImpl implements IntergalacticUnitCalculator {

    private HashMap<String, String> map;

    public IntergalacticUnitCalculatorImpl() {
        map = new HashMap<>();
    }

    public IntergalacticUnitCalculatorImpl(HashMap<String, String> map) {
        this.map = map;
    }

    public void setMap(HashMap<String, String> map) {
        this.map = map;
    }

    @Override
    public int valueOfUnit(String unit) {
        String roman = romanNumeralOfUnit(unit);
        return new RomanNumeral(roman).getValue();
    }

    @Override
    public void addUnitRomanMapEntry(String s) {
        if (s.matches(GalaxyNoteConstants.UNIT_ROMAN_MAP_REGEX)) {
            String[] strings = s.split(" is ");
            String unit = strings[0];
            String roman = strings[1];
            //todo 重复数据异常处理
            map.put(unit, roman);
        } else {
            throw new GalaxyException("illegal map: " + s);
        }
    }

    private String romanNumeralOfUnit(String unit) {
        StringBuilder roman = new StringBuilder();
        for (String u : unit.split(" ")) {
            String roman0 = romanOfUnit(u);
            if (roman0 == null) {
                throw new UnknownUnitException("unit=" + u);
            }
            roman.append(roman0);
        }
        if (roman.length() <= 0) {
            throw new UnknownUnitException("unit=" + unit);
        }
        return roman.toString();
    }

    private String romanOfUnit(String u) {
        return map == null ? null : map.get(u);
    }
}
