package two8g.galaxy.calculator;

/**
 * Created by two8g on 17-4-13.
 */
public interface IntergalacticUnitCalculator {
    int valueOfUnit(String unit);

    void addUnitRomanMapEntry(String s);
}
