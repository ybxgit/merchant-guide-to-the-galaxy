package two8g.galaxy.calculator;

/**
 * Created by two8g on 17-4-14.
 */
public interface CalculatorFactory {
    IntergalacticUnitCalculator getIntergalacticUnitCalculator();

    MerchandiseCalculator getMerchandiseCalculator();
}
