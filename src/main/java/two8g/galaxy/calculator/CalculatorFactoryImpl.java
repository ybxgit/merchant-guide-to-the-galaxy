package two8g.galaxy.calculator;

/**
 * Created by two8g on 17-4-14.
 */
public class CalculatorFactoryImpl implements CalculatorFactory {
    private static CalculatorFactoryImpl INSTANCE;

    static {
        INSTANCE = new CalculatorFactoryImpl();
    }

    private CalculatorFactoryImpl() {
    }

    public static CalculatorFactoryImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public IntergalacticUnitCalculator getIntergalacticUnitCalculator() {
        return new IntergalacticUnitCalculatorImpl();
    }

    @Override
    public MerchandiseCalculator getMerchandiseCalculator() {
        return new MerchandiseCalculatorImpl();
    }
}
