package two8g.galaxy.exception;

/**
 * Created by two8g on 17-4-14.
 */
public class ProcessException extends GalaxyException {
    public ProcessException() {
    }

    public ProcessException(String s) {
        super(s);
    }

    public ProcessException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProcessException(Throwable cause) {
        super(cause);
    }
}
