package two8g.galaxy.exception;

/**
 * Created by two8g on 17-4-13.
 */
public class InputReadException extends GalaxyException {
    public InputReadException() {
    }

    public InputReadException(String s) {
        super(s);
    }

    public InputReadException(String message, Throwable cause) {
        super(message, cause);
    }

    public InputReadException(Throwable cause) {
        super(cause);
    }
}
