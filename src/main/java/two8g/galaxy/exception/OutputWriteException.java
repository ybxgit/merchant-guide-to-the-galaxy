package two8g.galaxy.exception;

/**
 * Created by two8g on 17-4-14.
 */
public class OutputWriteException extends GalaxyException {
    public OutputWriteException() {
    }

    public OutputWriteException(String s) {
        super(s);
    }

    public OutputWriteException(String message, Throwable cause) {
        super(message, cause);
    }

    public OutputWriteException(Throwable cause) {
        super(cause);
    }
}
