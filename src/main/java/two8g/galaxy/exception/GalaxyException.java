package two8g.galaxy.exception;

/**
 * Created by two8g on 17-4-13.
 */
public class GalaxyException extends IllegalArgumentException {
    public GalaxyException() {
    }

    public GalaxyException(String s) {
        super(s);
    }

    public GalaxyException(String message, Throwable cause) {
        super(message, cause);
    }

    public GalaxyException(Throwable cause) {
        super(cause);
    }
}
