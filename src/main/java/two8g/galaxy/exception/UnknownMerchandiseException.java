package two8g.galaxy.exception;

/**
 * Created by two8g on 17-4-13.
 */
public class UnknownMerchandiseException extends GalaxyException {
    public UnknownMerchandiseException() {
    }

    public UnknownMerchandiseException(String s) {
        super(s);
    }

    public UnknownMerchandiseException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownMerchandiseException(Throwable cause) {
        super(cause);
    }
}
