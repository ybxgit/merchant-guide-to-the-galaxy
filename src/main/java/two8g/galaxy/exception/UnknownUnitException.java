package two8g.galaxy.exception;

/**
 * Created by two8g on 17-4-13.
 */
public class UnknownUnitException extends GalaxyException {
    public UnknownUnitException() {
    }

    public UnknownUnitException(String s) {
        super(s);
    }

    public UnknownUnitException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownUnitException(Throwable cause) {
        super(cause);
    }
}
