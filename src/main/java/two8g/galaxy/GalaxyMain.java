package two8g.galaxy;

import two8g.galaxy.exception.ProcessException;
import two8g.galaxy.process.FileInputReader;
import two8g.galaxy.process.FileOutputWriter;
import two8g.galaxy.process.MerchantGuideProcessor;
import two8g.galaxy.process.MerchantGuideProcessorImpl;

/**
 * application main class
 * Created by two8g on 17-4-13.
 */
public class GalaxyMain {
    /**
     * @param args optional args: [inputFilePath [outFilePath]]
     */
    public static void main(String[] args) {
        MerchantGuideProcessor merchantGuideProcessor = new MerchantGuideProcessorImpl();
        if (args != null) {
            if (args.length >= 1) {
                merchantGuideProcessor.setInputReader(new FileInputReader(args[0]));
            }
            if (args.length >= 2) {
                merchantGuideProcessor.setOutputWriter(new FileOutputWriter(args[1]));
            }
        }
        try {
            merchantGuideProcessor.process();
        } catch (ProcessException e) {
            System.out.println(e.getMessage());
        }
    }
}
