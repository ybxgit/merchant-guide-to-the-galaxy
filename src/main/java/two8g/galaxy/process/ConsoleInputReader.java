package two8g.galaxy.process;

import two8g.galaxy.exception.InputReadException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by two8g on 17-4-15.
 */
public class ConsoleInputReader implements InputReader {
    private BufferedReader scanner;

    public ConsoleInputReader() {
        scanner = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public String readLine() throws InputReadException {
        try {
            return scanner.readLine();
        } catch (IOException e) {
            throw new InputReadException(e);
        }
    }

    @Override
    public void close() throws IOException {
        if (scanner != null) {
            scanner.close();
        }
    }
}
