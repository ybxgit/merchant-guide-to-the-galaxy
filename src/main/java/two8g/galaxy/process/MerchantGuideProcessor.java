package two8g.galaxy.process;

import two8g.galaxy.exception.ProcessException;

/**
 * Created by two8g on 17-4-13.
 */
public interface MerchantGuideProcessor {
    void setInputReader(InputReader inputReader);

    void setOutputWriter(OutputWriter outputWriter);

    void process() throws ProcessException;
}
