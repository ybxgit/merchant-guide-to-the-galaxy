package two8g.galaxy.process;

import two8g.galaxy.exception.InputReadException;

import java.io.IOException;

/**
 * Created by two8g on 17-4-13.
 */
public interface InputReader {
    //List<String> getNotes();
    //List<String> getQueries();

    String readLine() throws InputReadException;

    void close() throws IOException;
}
