package two8g.galaxy.process;

import two8g.galaxy.exception.OutputWriteException;

/**
 * Created by two8g on 17-4-13.
 */
public interface OutputWriter {
    void writeOutput(String output) throws OutputWriteException;

    void done();
}
