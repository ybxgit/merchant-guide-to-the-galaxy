package two8g.galaxy.process;

/**
 * Created by two8g on 17-4-14.
 */
public class ConsoleOutputWriter implements OutputWriter {
    @Override
    public void writeOutput(String output) {
        System.out.println(output);
    }

    @Override
    public void done() {
        System.out.close();
    }
}
